# Paul Myers 2018

extends Node2D

export (PackedScene) var MenuOption

enum {LEFT,RIGHT,UP,DOWN}
enum {MENU, MOVE, ATTACK}
var default_options = ["Move","Attack","Wait"]
var menu_options = []
var highlighted = null

func _ready():
	pass

func add_menu_option(option, index):
	menu_options.insert(index,MenuOption.instance())
	menu_options[index].get_node("Option/Label").set_text(option)
	$MenuBox/Options.add_child(menu_options[index])
	reset_contents_pos()

# For demo
func set_menu_up_for_all_units():
	for i in range(default_options.size()):
		add_menu_option(default_options[i],menu_options.size())
	set_highlight(-1)

func set_menu_up_for_unit(unit):
	# clear last unit's unique commands, but there aren't any yet
	#for i in range(default_options.size(), menu_options.size()-1):
		#print("Shouldn't see this! i="+str(i))
		# TODO delete nodes of those menu options
	# add this unit's unique commands
	for i in range(unit.additional_menu_options.size()):
		add_menu_option(unit.additional_menu_options[i],menu_options.size()-1)
	reset_contents_pos()
	# If unit has no RT for certain actions, gray them out
	# TODO: Fudging this a bit, should actually go through all and check
	if (unit.max_RT-unit.RT_used)<unit.move_cost:
		menu_options[0].get_node("Option").set_modulate(Color(0.0,0.0,0.0,1.0))
	if (unit.max_RT-unit.RT_used)<unit.attack_cost:
		menu_options[1].get_node("Option").set_modulate(Color(0.0,0.0,0.0,1.0))
	# set menu position to this unit
	position_menu_for_unit(unit,MENU)
	set_highlight(-1)
	show()

func position_menu_for_unit(unit,mode):
	var side = [LEFT,UP]
	var menuRect = get_node("MenuBox").get_rect()
	var viewRect = Rect2(Vector2(-140,-40),Vector2(get_node("../..").get_viewport_rect().size)/2.2)
	set_position(Vector2(unit.position.x-(menuRect.size.x+20),unit.position.y-(menuRect.size.y-10)))
	# If menu is out of screen, flip to other side of unit
	if position.x < viewRect.position.x:
		set_position(Vector2(position.x+(menuRect.size.x+30), position.y))
		side[0] = RIGHT
	if position.y < viewRect.position.y:
		set_position(Vector2(position.x, position.y + (menuRect.size.y+10)))
		side[1] = DOWN
	if mode == ATTACK:
		var highlighted_direc = get_node("..").highlighted_direc
		set_position(Vector2(position.x, position.y + 30))
		if highlighted_direc == side[0]:
			if side[0] == LEFT:
				set_position(Vector2(position.x, position.y))
			if side[0] == RIGHT:
				set_position(Vector2(position.x, position.y))
func reset_contents_pos():
	# TODO reorder contents based on menu_options order
	# reset the contents of the nodes after adding selections
	for i in range(menu_options.size()):
		menu_options[i].get_node("Option").set_position(Vector2(0,0))
		menu_options[i].get_node("Option").set_modulate(Color(1.0,1.0,1.0,1.0))
	$MenuBox.set_position(Vector2(0,0))
	$MenuBox.set_size(Vector2(0,0))
	# set the 9 patch size to menu size
	$Back.set_size(Vector2($MenuBox.get_rect().size.x,$MenuBox.get_rect().size.y))
	$Border.set_size(Vector2($MenuBox.get_rect().size.x,$MenuBox.get_rect().size.y))

func set_highlight(direc):
	# unhighlight previous
	if highlighted != null:
		menu_options[highlighted].get_node("Highlight").set_modulate(Color(1.0,1.0,1.0,0.0))
	# check if highlight being set to default
	if direc == -1:
		menu_options[0].get_node("Highlight").set_modulate(Color(1.0,1.0,1.0,0.5))
		highlighted = 0
		return
	# Use direction to change highlighted
	if direc == DOWN:
		if highlighted == menu_options.size()-1:
			highlighted = 0
		else:
			highlighted += 1
	elif direc == UP:
		if highlighted == 0:
			highlighted = menu_options.size()-1
		else:
			highlighted -= 1
	menu_options[highlighted].get_node("Highlight").set_modulate(Color(1.0,1.0,1.0,0.5))

# Player selected a menu option
func menu_select():
	# TODO: should all be handled with signals
	var selection = menu_options[highlighted].get_node("Option/Label").text
	if selection == "Move":
		get_node("../..").to_move_mode()
	if selection == "Attack":
		get_node("../..").to_attack_mode()
	if selection == "Wait":
		get_node("../..").cur_unit_wait()