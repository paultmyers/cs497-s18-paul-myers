# Paul Myers 2018

extends Node2D

export (PackedScene) var PlayerUnit
export (PackedScene) var EnemyUnit
export (PackedScene) var FieldMenu

enum {MENU,MOVE,ATTACK,STATUS,FIELD}
enum {WALL,FLOOR,STAIR,PLAYER,ENEMY}
enum {LEFT,RIGHT,UP,DOWN}
# Directions to coordinates
var d_2_c = [[-1,0],[1,0],[0,-1],[0,1]]

onready var trail = $Movement/Trail
onready var attack_ui = $Attack_UI/Attack_Interface
# Demo battlefield map
onready var levels = [$Level1, $Level2]

var mode = -1
var cur_unit = null
var turnOrder = []
var numAdded = 0
var timeElapsed = 0

func _ready():
	$Menu.add_child(FieldMenu.instance())
	$Menu/FieldMenu.set_menu_up_for_all_units()
	$Menu/FieldMenu.hide()
	addUnit(PlayerUnit.instance(),0,6,14)
	addUnit(PlayerUnit.instance(),0,6,15)
	addUnit(PlayerUnit.instance(),0,6,16)
	addUnit(PlayerUnit.instance(),0,7,14)
	addUnit(PlayerUnit.instance(),0,7,16)
	addUnit(EnemyUnit.instance(),1,5,3)
	addUnit(EnemyUnit.instance(),1,5,6)
	addUnit(EnemyUnit.instance(),1,7,3)
	addUnit(EnemyUnit.instance(),1,9,3)
	addUnit(EnemyUnit.instance(),1,9,6)
	set_cur_unit(turnOrder[0])
	
func _process(delta):
	if mode == MENU:
		# Move up and down menu options
		if Input.is_action_just_pressed("ui_up"):
			$Menu/FieldMenu.set_highlight(UP)
		if Input.is_action_just_pressed("ui_down"):
			$Menu/FieldMenu.set_highlight(DOWN)
		# Menu option selected
		if Input.is_action_just_pressed("ui_accept"):
			$Menu/FieldMenu.menu_select()
		if Input.is_action_just_pressed("ui_cancel"):
			to_field_mode()
	elif mode == MOVE:
		# Navigate arrow with arrow keys
		if Input.is_action_just_pressed("ui_right"):
			trail.nav_arrow(RIGHT,cur_unit)
			trail.draw_path()
		if Input.is_action_just_pressed("ui_left"):
			trail.nav_arrow(LEFT,cur_unit)
			trail.draw_path()
		if Input.is_action_just_pressed("ui_up"):
			trail.nav_arrow(UP,cur_unit)
			trail.draw_path()
		if Input.is_action_just_pressed("ui_down"):
			trail.nav_arrow(DOWN,cur_unit)
			trail.draw_path()
		# Finalize movement
		if Input.is_action_just_pressed("ui_accept"):
			var new_pos = trail.moved_coords[trail.moved_coords.size()-1]
			# check if tile you are moving to, on the level you are moving to, is empty floor
			if trail.nav_arrow_level.traversable_tiles[new_pos[0]][new_pos[1]] != 1:
				return
			cur_unit.set_tile_pos(new_pos[0],new_pos[1],new_pos[2],trail.move_time)
			to_menu_mode()
		if Input.is_action_just_pressed("ui_cancel"):
			to_menu_mode()
	elif mode == ATTACK:
		if attack_ui.step == 1:
			if Input.is_action_just_pressed("ui_right"):
				attack_ui.highlight_direc(RIGHT)
			if Input.is_action_just_pressed("ui_left"):
				attack_ui.highlight_direc(LEFT)
			if Input.is_action_just_pressed("ui_up"):
				attack_ui.highlight_direc(UP)
			if Input.is_action_just_pressed("ui_down"):
				attack_ui.highlight_direc(DOWN)
			if Input.is_action_just_pressed("ui_accept"):
				attack_ui.to_step_two()
			if Input.is_action_just_pressed("ui_cancel"):
				to_menu_mode()
		elif attack_ui.step == 2:
			if Input.is_action_just_pressed("ui_accept"):
				attack_ui.execute_attack()
			if Input.is_action_just_pressed("ui_cancel"):
				to_attack_mode()
	elif mode == FIELD:
		if Input.is_action_just_pressed("ui_cancel"):
			to_menu_mode()

func addUnit(unit,height,row,col):
	get_node("Units").add_child(unit)
	unit.set_tile_pos(row,col,height,0)
	unit.unit_name = "unit"+str(numAdded)
	numAdded+=1
	placeInTurnOrder(unit)
	
func deleteUnit(unit):
	get_node("Units").remove_child(unit)
	levels[unit.tilePos[2]].traversable_tiles[unit.tilePos[0]][unit.tilePos[1]] = 1
	turnOrder.remove(unit.turn_index)

func set_cur_unit(unit):
	if cur_unit != null:
		cur_unit.unmake_current()
	cur_unit = unit
	cur_unit.make_current()
	$Menu/FieldMenu.set_menu_up_for_unit(cur_unit)
	to_menu_mode()

func cur_unit_wait():
	cur_unit.wait()
	placeInTurnOrder(cur_unit)
	# time passes until next unit's RT is 0
	var wait_time = turnOrder[0].RT
	timeElapsed += wait_time
	for i in range(turnOrder.size()):
		turnOrder[i].RT -= wait_time
		# debug: print(turnOrder[i].unit_name+"'s RT:"+str(turnOrder[i].RT))
	# debug: print("\n")
	set_cur_unit(turnOrder[0])

# Place a unit in turn order depending on its recovery time
func placeInTurnOrder(unit):
	# if you find the unit, copy array without it, taking it out in order to reinsert
	for i in range(turnOrder.size()):
		if  unit.unit_name == turnOrder[i].unit_name:
			var new_turn_order = []
			for j in range(turnOrder.size()-1):
				if j >= i:
					new_turn_order.append(turnOrder[j+1])
				else:
					new_turn_order.append(turnOrder[j])
			turnOrder = new_turn_order
		break
	# Place the unit back in turn order
	var next_unit = null
	for i in range(turnOrder.size()+1):
		if i == turnOrder.size():
			turnOrder.append(unit)
		elif unit.RT < turnOrder[i].RT:
			next_unit = turnOrder[i]
			turnOrder[i] = unit
			unit = next_unit
	# update the units' values
	for i in range(turnOrder.size()):
		turnOrder[i].set_turn_index(i)
		
func to_move_mode():
	# Don't bother if not enough RT to move a space
	if (cur_unit.max_RT - cur_unit.RT_used) < cur_unit.move_cost:
		return
	mode=MOVE
	$Menu/FieldMenu.hide()
	# reset nav arrow using current unit
	cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set("custom_colors/font_color",Color(0.2,0.4,1.0,1.0))
	trail.nav_arrow_level = levels[cur_unit.tilePos[2]]
	trail.moved_coords = [cur_unit.tilePos]
	trail.directions = [-1]
	trail.move_time = 0

func to_menu_mode():
	mode = MENU
	$Menu/FieldMenu.set_menu_up_for_unit(cur_unit)
	attack_ui.hide()
	trail.clear()
	# Set up RT_Counter
	cur_unit.get_node("RT_Counter").show()
	cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set_text(str(cur_unit.RT_used))
	cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set("custom_colors/font_color",Color(0.2,0.8,0.2,1.0))
	if cur_unit.RT_used >= cur_unit.max_RT:
		cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set("custom_colors/font_color",Color(0.8,0.3,0.3,1.0))
		cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/slash").set("custom_colors/font_color",Color(0.8,0.3,0.3,1.0))
		cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/den").set("custom_colors/font_color",Color(0.8,0.3,0.3,1.0))

# TODO: move cursor around field, inspect tile properties, other units 
func to_field_mode():
	mode = FIELD
	$Menu/FieldMenu.hide()

# TODO: attacking
func to_attack_mode():
	# Don't bother if not enough RT to attack
	if (cur_unit.max_RT - cur_unit.RT_used) < cur_unit.attack_cost:
		return
	mode = ATTACK
	$Menu/FieldMenu.hide()
	cur_unit.get_node("RT_Counter").hide()
	attack_ui.show()
	attack_ui.set_to_unit(cur_unit)
