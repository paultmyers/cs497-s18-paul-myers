# Paul Myers 2018

extends Sprite

var unit_name = null
var type = null
# row, col, height
var tilePos = [0,0,0]
var turn_index = null

##### stats for unbalanced placeholders for demo #####
var stats = {
	"max_HP" :20.0,"max_MP":20.0,"SP":20,
	"ATP":20.0,"ACC":0.0,"EVA":0.0,
	"MST":20.0,"FOC":0.0,"RES":0.0}
var HP = stats["max_HP"]
var MP = stats["max_MP"]
var RT = 0
var min_RT = 14
var max_RT = stats["SP"]
var move_cost = 2
var attack_cost = 6
var attack_damage = 7
######################################################
var RT_used = 0
var additional_menu_options = []

func _ready():
	pass

func make_current():
	get_node("Sprite").modulate -= Color(0.3,0.3,0.3,0.0)
	get_node("TurnLabel").hide()
	get_node("RT_Counter").show()
	# Set the RT Counter to default values + color
	get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set_text(str(RT_used))
	get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/den").set_text(str(max_RT))
	get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set("custom_colors/font_color",Color(0.2,0.8,0.2,1.0))
	get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/slash").set("custom_colors/font_color",Color(0.2,0.8,0.2,1.0))
	get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/den").set("custom_colors/font_color",Color(0.2,0.8,0.2,1.0))
	z_index += 1.0
	
func unmake_current():
	get_node("Sprite").modulate += Color(0.3,0.3,0.3,0.0)
	get_node("TurnLabel").show()
	get_node("RT_Counter").hide()
	z_index -= 1.0

# Use turn's incurred RT to calculate RT
func wait():
	if RT_used < min_RT:
		RT = min_RT
	else:
		RT = RT_used
	RT_used = 0

func set_tile_pos(row,col,height,RT_incurred):
	get_node("../..").levels[tilePos[2]].traversable_tiles[tilePos[0]][tilePos[1]] = 1
	tilePos = [row,col,height]
	get_node("../..").levels[height].traversable_tiles[row][col] = type
	position.y = row*20 + 10 - 40
	position.x = col*20 + 10 - 140
	z_index = get_node("../..").levels[height].get_node("Walls").z_index
	RT_used += RT_incurred
	# debug: print("x: "+str(tilePos[1])+" y: "+str(tilePos[0])+" z: "+str(z_index))

func set_turn_index(index):
	turn_index = index
	get_node("TurnLabel/Label").set_text(str(index))
	# debug: print("set "+unit_name+" to index "+str(turn_index))