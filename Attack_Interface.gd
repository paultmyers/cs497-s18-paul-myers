# Paul Myers 2018

extends Node2D

enum {MENU, MOVE, ATTACK}
enum {LEFT,RIGHT,UP,DOWN}
# Directions to coordinates
var d_2_c = [[-1,0],[1,0],[0,-1],[0,1]]

onready var highlight_panels = [$Highlight/AtkW,$Highlight/AtkE,$Highlight/AtkN,$Highlight/AtkS]
onready var option_panels = [$Options/AtkW,$Options/AtkE,$Options/AtkN,$Options/AtkS]

var step = 0
var tile_position = [0,0,0]
var level = null
var highlighted_direc = null
var highlightable_direcs = [0,0,0,0]
var targeted_unit = null

func _ready():
	$Attack_Confirm.add_menu_option("Attack?",0)
	#$Attack_Confirm.add_menu_option("Cancel",1)
	$Attack_Confirm.reset_contents_pos()
	$Attack_Info/Target_HP.add_menu_option("",0)
	$Attack_Info/DMG.add_menu_option("",0)
	pass
	
func set_to_unit(unit):
	step = 1
	targeted_unit = null
	hide_all_panels()
	# place panels in right screen location
	$Highlight.set_position(Vector2(unit.position.x-30,unit.position.y-30))
	$Options.set_position(Vector2(unit.position.x-30,unit.position.y-30))
	$Attack_Info.set_position(Vector2(unit.position.x-100,unit.position.y-60))
	# set to same coordinates as unit
	tile_position = unit.tilePos
	level = get_node("../..").levels[tile_position[2]]
	# check 4 adjacent tiles and hide ones that aren't options
	for i in range(option_panels.size()):
		if !tile_in_direc(i):
			continue
		if tile_in_direc(i) == 1 || tile_in_direc(i) >= 3:
			option_panels[i].show()
			highlightable_direcs[i] = 1
	highlighted_direc = -1

func to_step_two():
	targeted_unit = get_highlighted_unit_info()
	if !get_highlighted_unit_info():
		return
	if highlighted_direc == -1:
		return
	step = 2
	hide_all_panels()
	highlight_panels[highlighted_direc].show()
	$Attack_Confirm.position_menu_for_unit(get_node("../..").cur_unit,ATTACK)
	set_info_position()
	$Attack_Confirm.show()
	$Attack_Info.show()
	
func execute_attack():
	get_node("../..").cur_unit.RT_used += get_node("../..").cur_unit.attack_cost
	targeted_unit.HP -= get_node("../..").cur_unit.attack_damage
	if targeted_unit.HP <= 0:
		get_node("../..").deleteUnit(targeted_unit)
	get_node("../..").to_menu_mode()

func highlight_direc(direc):
	if highlightable_direcs[direc] == 1:
		if highlighted_direc != -1:
			highlight_panels[highlighted_direc].hide()
			option_panels[highlighted_direc].show()
		highlighted_direc = direc
		option_panels[direc].hide()
		highlight_panels[direc].show()
		highlighted_direc = direc

func tile_in_direc(direc):
	if (tile_position[0]+d_2_c[direc][1]) in range(16) && tile_position[1]+d_2_c[direc][0] in range(19):
		return level.traversable_tiles[tile_position[0]+d_2_c[direc][1]][tile_position[1]+d_2_c[direc][0]]
	else:
		return false
	
func hide_all_panels():
	for i in range(option_panels.size()):
		highlight_panels[i].hide()
		option_panels[i].hide()
		highlightable_direcs[i] = 0
	$Attack_Confirm.hide()
	$Attack_Info.hide()
	
func get_highlighted_unit_info():
	if tile_in_direc(highlighted_direc) >= 3:
		var units = get_node("../..").turnOrder
		#print("!!!")
		for i in range(units.size()):
			#print(str(units[i].tilePos)+" !!! "+str([tile_position[0]+d_2_c[highlighted_direc][1],tile_position[0]+d_2_c[highlighted_direc][0],level.height]))
			if units[i].tilePos == [tile_position[0]+d_2_c[highlighted_direc][1],tile_position[1]+d_2_c[highlighted_direc][0],level.height]:
				$Attack_Info/Target_HP/MenuBox/Options/MenuOption/Option/Label.set_text("HP:"+str(units[i].HP)+"/"+str(units[i].stats["max_HP"]))
				$Attack_Info/DMG/MenuBox/Options/MenuOption/Option/Label.set_text("Dmg:"+str(units[i].attack_damage))
				$Attack_Info/Target_HP.reset_contents_pos()
				$Attack_Info/DMG.reset_contents_pos()
				return units[i]
	return

func set_info_position():
	var viewRect = Rect2(Vector2(-140,-40),Vector2(get_node("../..").get_viewport_rect().size)/2.2)
	var infoRect = $Attack_Info.get_rect()
	if infoRect.position.x < viewRect.position.x:
		$Attack_Info.set_position(Vector2(infoRect.position.x+110, infoRect.position.y))
	if infoRect.position.y < viewRect.position.y:
		$Attack_Info.set_position(Vector2(infoRect.position.x, infoRect.position.y + (20+10)))