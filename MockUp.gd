extends Node2D

export (PackedScene) var PlayerUnit
export (PackedScene) var EnemyUnit
export (PackedScene) var FieldMenu

onready var levels = [$Level1, $Level2]
onready var trail = $Movement/Trail

var cur_unit = null
var turnOrder = []
enum {WALL,FLOOR,STAIR,PLAYER,ENEMY}
enum {LEFT,RIGHT,UP,DOWN}
enum {MENU, MOVE, ATTACK}
var mode = MENU
# Directions to coordinates
var d_2_c = [[-1,0],[1,0],[0,-1],[0,1]]
var numAdded = 0
var timeElapsed = 0
var map_transform = Vector2(7,2)

func _ready():
	$Menu.add_child(FieldMenu.instance())
	$Menu/FieldMenu.hide()
	addUnit(PlayerUnit.instance(),0,6,14)
	addUnit(PlayerUnit.instance(),0,6,15)
	addUnit(PlayerUnit.instance(),0,6,16)
	addUnit(PlayerUnit.instance(),0,7,14)
	addUnit(PlayerUnit.instance(),0,7,16)
	addUnit(EnemyUnit.instance(),1,5,3)
	addUnit(EnemyUnit.instance(),1,5,6)
	addUnit(EnemyUnit.instance(),1,7,3)
	addUnit(EnemyUnit.instance(),1,9,3)
	addUnit(EnemyUnit.instance(),0,9,15)
	set_cur_unit(turnOrder[0])
	
func _process(delta):
	# User pressed enter
	if Input.is_action_just_pressed("ui_accept"):
		# Finalize movement
		if mode == MOVE:
			var new_pos = cur_unit.moved_coords[cur_unit.moved_coords.size()-1]
			# check if tile you are moving to, on the level you are moving to, is empty floor
			if cur_unit.nav_arrow_level.traversable_tiles[new_pos[0]][new_pos[1]] != 1:
				return
			cur_unit.set_tile_pos(new_pos[0],new_pos[1],new_pos[2])
			# Switch back to menu
			trail.clear()
			mode = MENU
			cur_unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set("custom_colors/font_color",Color(0.2,0.8,0.2,1.0))
			$Menu/FieldMenu.set_menu_up_for_unit(cur_unit)
		# Player chose a menu option
		elif mode == MENU:
			$Menu/FieldMenu.menu_select()
#		
	if mode == MENU:
		if Input.is_action_just_pressed("ui_up"):
			$Menu/FieldMenu.set_highlight(UP)
		if Input.is_action_just_pressed("ui_down"):
			$Menu/FieldMenu.set_highlight(DOWN)
	if mode == MOVE:
		# Move your unit
		if Input.is_action_just_pressed("ui_right"):
			cur_unit.nav_arrow(RIGHT)
			trail.draw_path(cur_unit)
		if Input.is_action_just_pressed("ui_left"):
			cur_unit.nav_arrow(LEFT)
			trail.draw_path(cur_unit)
		if Input.is_action_just_pressed("ui_up"):
			cur_unit.nav_arrow(UP)
			trail.draw_path(cur_unit)
		if Input.is_action_just_pressed("ui_down"):
			cur_unit.nav_arrow(DOWN)
			trail.draw_path(cur_unit)

func addUnit(unit,height,row,col):
	get_node("Units").add_child(unit)
	unit.set_tile_pos(row,col,height)
	unit.unit_name = "unit"+str(numAdded)
	numAdded+=1
	placeInTurnOrder(unit)

func set_cur_unit(unit):
	if cur_unit != null:
		cur_unit.unmake_current()
	cur_unit = unit
	cur_unit.make_current()
	$Menu/FieldMenu.set_menu_up_for_unit(cur_unit)

func cur_unit_wait():
	cur_unit.wait()
	placeInTurnOrder(cur_unit)
	# time passes until next unit's RT is 0
	var wait_time = turnOrder[0].RT
	timeElapsed += wait_time
	for i in range(turnOrder.size()):
		turnOrder[i].RT -= wait_time
		print(turnOrder[i].unit_name+"'s RT:"+str(turnOrder[i].RT))
	print("\n")
	set_cur_unit(turnOrder[0])

# Place a unit in turn order depending on its recovery time
func placeInTurnOrder(unit):
	# if you find the unit, copy array without it, taking it out in order to reinsert
	for i in range(turnOrder.size()):
		if  unit.unit_name == turnOrder[i].unit_name:
			var new_turn_order = []
			for j in range(turnOrder.size()-1):
				if j >= i:
					new_turn_order.append(turnOrder[j+1])
				else:
					new_turn_order.append(turnOrder[j])
			turnOrder = new_turn_order
		break
	# Place the unit back in turn order
	var next_unit = null
	for i in range(turnOrder.size()+1):
		if i == turnOrder.size():
			turnOrder.append(unit)
		elif unit.RT < turnOrder[i].RT:
			next_unit = turnOrder[i]
			turnOrder[i] = unit
			unit = next_unit
	# update the units' values
	for i in range(turnOrder.size()):
		turnOrder[i].set_turn_index(i)