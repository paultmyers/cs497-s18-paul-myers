README File for CS497 Special Project

Paul Myers - 

This is a rudimentary game which I have programmed over my Summer 2018 semester at Illinois Institute of Technology.

The game was developed with the Godot 3.0 engine for videogame development. All the code I wrote is in the *.gd godotscript files.

RUN THE GAME:

In order to compile the code in this directory, you must import it as a project to the Godot 3.0 software found here (page for the linux version):

https://godotengine.org/download/linux

Opening the Godot software will send you to the project manager. Click the import button on the right, and in the pop up window, click browse. This will allow you to navigate your file system to this current directory. There, select the file:

project.godot

for import. My project should now appear in your manager, and you can open it. Once opened, clicking the Arrow Play Button in the top right of the development environment will compile the project and run it.

Also included in this directory in the "exports" folder is the project pre-exported and executable for Mac, Windows, and Linux. Simply double-clicking them should run the game.

CONTROLS:

While playing the game, the controls are:

Arrow keys: to traverse the menus and navigate the movement arrow
Enter key: confirms menu slections, movement selections, and attack selections
Backspace/Escpae: Backspace will close and reopen the main unit menu, cancel movement, and also cancel attacking.

MECHANICS:

At the moment, this is a 2 player competitive strategy game. The two players control a Blue team and a Red team respectively.

Each of the ten units on the battlefield are able to move around and attack one another. 

When a unit's turn starts, you can see that they have taken spent 0/20 of their available points for making actions. Moving spaces and attacking increases the amount of strain on a unit. The more actions a unit takes in a turn, the longer until their next turn will arrive in the rotation.

The turn order of units is marked on the lower left of each unit.

When a unit attacks another, the targeted unit is dealt damage and their Hit Points drop. When a unit's HP drops to zero, it is removed from the battlefield. Once all of a team's units are removed, they lose the game.