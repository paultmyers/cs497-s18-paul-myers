# Paul Myers 2018

extends TileMap

# enum for tiles
enum {WALL,FLOOR,STAIR,PLAYER,ENEMY}
enum {LEFT,RIGHT,UP,DOWN}
# Directions->to->coordinates
var d_2_c = [[-1,0],[1,0],[0,-1],[0,1]]

# Arrow nav values
var moved_coords = null
var directions = null
var nav_arrow_level = null
var move_time = null

# Sprite mapping to directions
var direcs_to_base = {UP:4,DOWN:5,RIGHT:6,LEFT:7}
var direcs_to_arrows = {UP:0,DOWN:1,RIGHT:2,LEFT:3}
var direcs_to_con = {[DOWN,RIGHT]:8,[LEFT,UP]:8,[UP,RIGHT]:9,[LEFT,DOWN]:9,
	[RIGHT,DOWN]:10,[UP,LEFT]:10,[RIGHT,UP]:11,[DOWN,LEFT]:11,
	[RIGHT,RIGHT]:12,[LEFT,LEFT]:12,[UP,UP]:13,[DOWN,DOWN]:13,
	[DOWN,UP]:4,[UP,DOWN]:5,[LEFT,RIGHT]:6,[RIGHT,LEFT]:7}

# take an input direction and navigate arrow to it
func nav_arrow(direc,unit):
	var new_pos = [moved_coords[moved_coords.size()-1][0]+d_2_c[direc][1],
					moved_coords[moved_coords.size()-1][1]+d_2_c[direc][0]]
	# value to remember if arrow was nav'd accross stairs
	var pre_stairs_nav_arrow_level = null
	var tiles = nav_arrow_level.traversable_tiles
	# 1. check if its even on the grid
	if  !new_pos[0] in range(tiles.size()) || !new_pos[1] in range(tiles[0].size()):
		return
	# 2. If nav'd tile is stairs, use level's corresponding stair key
	#    to nav to level and tile stairs lead to
	if tiles[new_pos[0]][new_pos[1]] == STAIR:
		var stair_key = nav_arrow_level.stairs[new_pos]
		# check if you are right direction for stairs
		if stair_key[3] != direc:
			return
		# different level and tile is nav'd to
		pre_stairs_nav_arrow_level = nav_arrow_level
		new_pos = [new_pos[0] + stair_key[1], new_pos[1] + stair_key[2]]
		nav_arrow_level = get_node("../..").levels[stair_key[0]]
		tiles = nav_arrow_level.traversable_tiles
	# 3. If you've nav'd to this tile before, revert path to it
	for i in range(moved_coords.size()):
		if moved_coords[i] == [new_pos[0], new_pos[1], nav_arrow_level.height]:
			var new_coords = []
			var new_direcs = []
			for j in range(i+1):
				new_coords.append(moved_coords[j])
				new_direcs.append(directions[j])
			moved_coords = new_coords
			directions = new_direcs
			move_time = unit.move_cost*(moved_coords.size()-1)
			unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set_text(str(unit.RT_used+move_time))
			return
	# 4. New tile, can unit even nav here?
	if  tiles[new_pos[0]][new_pos[1]] == FLOOR || tiles[new_pos[0]][new_pos[1]] == unit.type:
		# Tile is good, can unit move any farther?
		if unit.RT_used + move_time + unit.move_cost > unit.max_RT:
			# TODO: If cant move farther, try to find a more efficient path to the destination tile
			pass
		else:
			# Yay! Add this new tile to path
			move_time += unit.move_cost
			moved_coords.append([new_pos[0],new_pos[1],nav_arrow_level.height])
			directions.append(direc)
			unit.get_node("RT_Counter/MarginContainer/MarginContainer/HBoxContainer/num").set_text(str(unit.RT_used+move_time))
			return
	# if used stairs but didn't nav, revert to pre-stairs level
	if pre_stairs_nav_arrow_level != null:
		nav_arrow_level = pre_stairs_nav_arrow_level

func draw_path():
	clear()
	if directions.size() == 1:
		return
	for i in range(1,moved_coords.size()):
		if i == moved_coords.size()-1:
			set_cell(moved_coords[i][1], moved_coords[i][0],
			direcs_to_arrows[directions[i]],false,false,false,Vector2(0,0))
		else:
			set_cell(moved_coords[i][1], moved_coords[i][0],
			direcs_to_con[[directions[i],directions[i+1]]],false,false,false,Vector2(0,0))